FROM rust

RUN apt-get update -y -qq && \
    apt-get install --assume-yes --no-install-recommends \
        ca-certificates \
        clang \
        emacs \
        libclang-dev \
        libsofthsm2 \
        libsqlite3-dev \
        libssl-dev \
        nettle-dev \
        make \
        opensc \
        org-mode \
        pkg-config \
        && \
    apt-get clean

RUN cargo install sequoia-sq

COPY Cargo.toml Cargo.lock /app/
COPY src /app/src
WORKDIR /app

RUN cargo build

RUN cargo test

COPY README.org /app/

RUN emacs -Q --batch --eval " \
    (progn \
      (require 'ob-tangle) \
      (dolist (file command-line-args-left) \
        (with-current-buffer (find-file-noselect file) \
          (org-babel-tangle))))" README.org

RUN chmod +x README.sh

RUN ./README.sh
