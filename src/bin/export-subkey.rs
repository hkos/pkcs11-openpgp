use cryptoki::types::object::ObjectClass;
use cryptoki::types::session::UserType;
use cryptoki::types::Flags;

use cryptoki::types::object::{Attribute, AttributeType};
use std::io;
use std::time::SystemTime;

use openpgp::packet::key::{Key4, PublicParts, SubordinateRole};
use openpgp::serialize::Serialize;
use openpgp::{
    crypto::mpi,
    packet::{Key, Packet},
};
use sequoia_openpgp as openpgp;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "export-subkey")]
struct Opt {
    /// PKCS#11 module to load
    #[structopt(short, long)]
    module: String,

    /// Serial number of the token to use
    #[structopt(short, long)]
    serial_number: String,

    /// PIN to access the card
    #[structopt(short, long, default_value = "112233")]
    pin: String,

    #[structopt(long, default_value = "0")]
    index: usize,

    #[structopt(short, long)]
    id: u8,

    #[structopt(long)]
    ecdh: bool,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let (pkcs11, slot) = pkcs11_openpgp::init_pins(&opt.module, &opt.serial_number, &opt.pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    // pub key template
    let pub_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(false.into()),
        Attribute::Class(ObjectClass::PUBLIC_KEY),
        Attribute::Id(vec![opt.id]),
    ];

    let mut objects = session.find_objects(&pub_key_template)?;

    eprintln!("{:?}", objects);

    for object in &objects {
        eprintln!("Object: {:?}", object);
        let attributes = session.get_attributes(*object, &[AttributeType::Label]);
        eprintln!("Attributes: {:?}", attributes);
        if let Ok(values) = attributes {
            if let Attribute::Label(str) = &values[0] {
                eprintln!(" STR = {}", String::from_utf8_lossy(&str));
            }
        }
        eprintln!();
    }

    let object = objects.remove(opt.index);

    let attributes = session.get_attributes(
        object,
        &[
            AttributeType::Modulus,
            AttributeType::PublicExponent,
            AttributeType::Label,
            AttributeType::EcPoint,
            AttributeType::KeyType,
        ],
    )?;

    let mut key_modulus: Option<Vec<u8>> = None;
    let mut key_exponent: Option<Vec<u8>> = None;
    let mut key_point: Option<Vec<u8>> = None;

    for attribute in attributes {
        match attribute {
            Attribute::Modulus(modulus) => {
                eprintln!("GOT MODULUS = {:X?}", modulus);
                key_modulus = Some(modulus)
            }
            Attribute::PublicExponent(exponent) => {
                eprintln!("GOT EXPONENT = {:X?}", exponent);
                key_exponent = Some(exponent);
            }
            Attribute::EcPoint(point) => {
                eprintln!("GOT POINT = {:X?} ({})", point, point.len());
                // DER decode point
                // https://bugzilla.mozilla.org/show_bug.cgi?id=480280
                let mut p = vec![0x04];
                p.extend(&point[3..]);
                eprintln!("CNV POINT = {:X?} ({})", p, p.len());
                key_point = Some(p);
            }
            Attribute::KeyType(key_type) => {
                eprintln!("GOT KEYTYPE = {:?}", key_type);
            }
            Attribute::Label(str) => eprintln!("USING = {}", String::from_utf8_lossy(&str)),
            _ => {}
        }
    }

    let key4: Key<PublicParts, SubordinateRole> =
        if let (Some(modulus), Some(exponent)) = (key_modulus, key_exponent) {
            Key4::import_public_rsa(&exponent, &modulus, SystemTime::UNIX_EPOCH)?.into()
        } else if let Some(point) = key_point {
            if opt.ecdh {
                Key4::new(
                    SystemTime::UNIX_EPOCH,
                    openpgp::types::PublicKeyAlgorithm::ECDH,
                    mpi::PublicKey::ECDH {
                        curve: openpgp::types::Curve::NistP256,
                        q: point.into(),
                        hash: openpgp::types::HashAlgorithm::SHA256,
                        sym: openpgp::types::SymmetricAlgorithm::AES256,
                    },
                )?
                .into()
            } else {
                Key4::new(
                    SystemTime::UNIX_EPOCH,
                    openpgp::types::PublicKeyAlgorithm::ECDSA,
                    mpi::PublicKey::ECDSA {
                        curve: openpgp::types::Curve::NistP256,
                        q: point.into(),
                    },
                )?
                .into()
            }
        } else {
            panic!("did not get both");
        };

    let packet: Packet = key4.into();
    let mut stdout = io::stdout();
    packet.serialize(&mut stdout)?;

    Ok(())
}
