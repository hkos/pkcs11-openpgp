use cryptoki::types::object::ObjectClass;
use cryptoki::types::session::UserType;
use cryptoki::types::Flags;

use cryptoki::types::object::{Attribute, AttributeType};
use pkcs11_openpgp::AlgoMechanism;
use std::io;
use std::sync::{Arc, Mutex};
use std::time::SystemTime;

use openpgp::packet::key::{Key4, PublicParts, SubordinateRole};
use openpgp::parse::Parse;
use openpgp::serialize::Marshal;
use openpgp::types::{HashAlgorithm, SignatureType};
use openpgp::Cert;
use openpgp::{
    crypto::mpi,
    packet::{signature, Key},
};
use sequoia_openpgp as openpgp;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "create-backsig")]
struct Opt {
    /// PKCS#11 module to load
    #[structopt(short, long)]
    module: String,

    /// Serial number of the token to use
    #[structopt(short, long)]
    serial_number: String,

    /// PIN to access the card
    #[structopt(short, long, default_value = "112233")]
    pin: String,

    #[structopt(long, default_value = "0")]
    index: usize,

    #[structopt(short, long)]
    id: u8,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();
    let stdin = io::stdin();
    let cert = Cert::from_reader(stdin)?;

    let (pkcs11, slot) = pkcs11_openpgp::init_pins(&opt.module, &opt.serial_number, &opt.pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    // pub key template
    let pub_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(false.into()),
        Attribute::Id(vec![opt.id]),
        Attribute::Class(ObjectClass::PUBLIC_KEY),
    ];

    let mut objects = session.find_objects(&pub_key_template)?;

    eprintln!("{:?}", objects);

    for object in &objects {
        eprintln!("Object: {:?}", object);
        let attributes = session.get_attributes(*object, &[AttributeType::Label]);
        eprintln!("Attributes: {:?}", attributes);
        if let Ok(values) = attributes {
            if let Attribute::Label(str) = &values[0] {
                eprintln!(" STR = {}", String::from_utf8_lossy(&str));
            }
        }
        eprintln!();
    }

    let object = objects.remove(opt.index);

    let attributes = session.get_attributes(
        object,
        &[
            AttributeType::Modulus,
            AttributeType::PublicExponent,
            AttributeType::Label,
            AttributeType::EcPoint,
            AttributeType::KeyType,
        ],
    )?;

    let mut key_modulus: Option<Vec<u8>> = None;
    let mut key_exponent: Option<Vec<u8>> = None;
    let mut key_point: Option<Vec<u8>> = None;

    for attribute in attributes {
        match attribute {
            Attribute::Modulus(modulus) => {
                eprintln!("GOT MODULUS = {:X?}", modulus);
                key_modulus = Some(modulus)
            }
            Attribute::PublicExponent(exponent) => {
                eprintln!("GOT EXPONENT = {:X?}", exponent);
                key_exponent = Some(exponent);
            }
            Attribute::EcPoint(point) => {
                eprintln!("GOT POINT = {:X?}", point);
                // DER decode point
                // https://bugzilla.mozilla.org/show_bug.cgi?id=480280
                let mut p = Vec::new();
                p.extend(&point[2..]);
                eprintln!("CNV POINT = {:X?} ({})", p, p.len());
                key_point = Some(p);
            }
            Attribute::KeyType(key_type) => {
                eprintln!("GOT KEY TYPE = {:?}", key_type);
            }
            Attribute::Label(str) => eprintln!("USING = {}", String::from_utf8_lossy(&str)),
            _ => {}
        }
    }

    let key4: (Key<PublicParts, SubordinateRole>, AlgoMechanism) =
        if let (Some(modulus), Some(exponent)) = (key_modulus, key_exponent) {
            (
                Key4::import_public_rsa(&exponent, &modulus, SystemTime::UNIX_EPOCH)?.into(),
                AlgoMechanism::Rsa,
            )
        } else if let Some(point) = key_point {
            (
                Key4::new(
                    SystemTime::UNIX_EPOCH,
                    openpgp::types::PublicKeyAlgorithm::ECDSA,
                    mpi::PublicKey::ECDSA {
                        curve: openpgp::types::Curve::NistP256,
                        q: point.into(),
                    },
                )?
                .into(),
                AlgoMechanism::Ecdsa,
            )
        } else {
            panic!("did not get both");
        };

    // priv key template
    let priv_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(true.into()),
        Attribute::Sign(true.into()),
        Attribute::Id(vec![opt.id]),
    ];

    let objects = session.find_objects(&priv_key_template)?;

    eprintln!("prv = {:?}, size = {}", objects, objects.len());

    for object in &objects {
        eprintln!("prv = Object: {:?}", object);
        let attributes = session.get_attributes(
            *object,
            &[AttributeType::Label, AttributeType::AllowedMechanisms],
        );
        eprintln!("prv = Attributes: {:?}", attributes);
        if let Ok(values) = attributes {
            if let Attribute::Label(str) = &values[0] {
                eprintln!("prv = STR = {}", String::from_utf8_lossy(&str));
            }
        }
        eprintln!();
    }

    let mut subkey_signer = pkcs11_openpgp::PkcsKeyPair::new(
        &key4.0.role_as_unspecified(),
        &objects[0],
        Arc::new(Mutex::new(session)),
        key4.1,
    );

    let backsig = signature::SignatureBuilder::new(SignatureType::PrimaryKeyBinding)
        .set_signature_creation_time(SystemTime::now())?
        .set_hash_algo(HashAlgorithm::SHA512)
        .sign_primary_key_binding(&mut subkey_signer, &cert.primary_key(), &key4.0)?;

    let mut stdout = io::stdout();
    backsig.serialize(&mut stdout)?;

    Ok(())
}
